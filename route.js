const db = require('./conn')
const router = require('express').Router()

const express = require('express')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)

router.get('/', (req, res) => {
    res.render('guest/index')
})

router.get('/create-nomor-antrean-manual', (req, res) => {
    db.query("TRUNCATE TABLE antrean_lokets")
    
    for (var i = 1; i <= 500; i++) {
        db.query(`INSERT INTO antrean_lokets (operator, number, time) VALUES ('0', '${i}', '0')`)
    }
    res.render('guest/index')
})

router.post('/api/antrean/create', (req, res) => {
    db.query(`SELECT number FROM antrean_lokets ORDER BY id DESC LIMIT 1`, (err, result) => {
        if (result.length != 0) {
            db.query(`INSERT INTO antrean_lokets (operator, number, time) VALUES ('0', '${result[0].number + 1}', '0')`)
        } else {
            db.query(`INSERT INTO antrean_lokets (operator, number, time) VALUES ('0', '1', '0')`)
        }
        res.send('BERHASIL')
    })
})

// route
router.get('/operator', (req, res) => {
    db.query("SELECT number FROM antrean_lokets WHERE operator='" + req.cookies.operatorAntrean + "' ORDER BY time DESC", (err, results) => {
        res.render('operator/index', {
            title: 'Selamat Bertugas Operator!',
            antrean_lokets: results
        })
    })
})

router.get('/api/antrean/operator/:operator', (req, res) => {
    db.query("SELECT number FROM antrean_lokets WHERE operator='" + req.params.operator + "' ORDER BY time DESC LIMIT 0,5", (err, results) => {
        if (err) throw err;
        res.json(results)
    })
})

router.post('/operator', (req, res) => {
    db.query("UPDATE antrean_lokets SET operator='" + req.body.operator + "', time='" + Date.now() + "' WHERE (operator='0' OR operator='" + req.body.operator + "') AND number='" + req.body.number + "'")
})

router.post('/operator/next', (req, res) => {
    db.query("SELECT number FROM antrean_lokets WHERE operator='0' LIMIT 0,1", (err, results) => {
        var number = 0;
        if (results.length) {
            number = results[0].number
        }
        res.json({
            number
        })
    })
})

router.post('/operator/next/query', (req, res) => {
    db.query("UPDATE antrean_lokets SET operator='" + req.body.operator + "', time='" + Date.now() + "' WHERE number='" + req.body.number + "'")
})

// ajax
router.get('/ajax/guest/table-queue', (req, res) => {
    db.query("SELECT number FROM antrean_lokets WHERE operator = ? ORDER BY time DESC LIMIT 0,1;SELECT number FROM antrean_lokets WHERE operator = ? ORDER BY time DESC LIMIT 0,1;SELECT number FROM antrean_lokets WHERE operator = ? ORDER BY time DESC LIMIT 0,1;SELECT number FROM antrean_lokets WHERE operator = ? ORDER BY time DESC LIMIT 0,1;SELECT number FROM antrean_lokets WHERE operator = ? ORDER BY time DESC LIMIT 0,1;SELECT number FROM antrean_lokets WHERE operator = ? ORDER BY time DESC LIMIT 0,1; SELECT number from antrean_lokets WHERE operator = '0' limit 0,3", ['1', '2', '3', '4', '5', '6'], (err, results) => {
        res.render('ajax/table-queue', {
            op: results,
            layout: false
        })
    })
})

// antrean_lokets reset
router.get('/reset-data', (req, res) => {
    db.query("UPDATE antrean_lokets SET operator='0', time='0'")
    res.render('guest/index')
})

module.exports = router
